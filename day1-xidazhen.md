day1-xidazhen

**O:** I got to know each other with thoughtworks teachers this morning, after which I learned about the main course content and arrangement of this training camp. By watching the ted video about learning, I learned the factors that affect the learning efficiency, and then learned the PDCA method. I can divide everything into a cycle composed of Plan, Do, Check and Action, and put it into action on the basis of establishing a clear plan, and record my completion degree in time to reflect on my bad work. And the content of the summary is revised to the action, so as to complete the efficient action. In the afternoon, I mainly introduced the content of concept map, completed a demo with my team, and then learned the ORID method.

**R:** meaningful

**I:** The most meaningful thing is to share a lot of ways to improve efficiency, whether in study or at work, learning methods are far more important than efforts.

**D:** The PDCA method I learned today will be used in reading technical books, while the ORID method will be reflected in Daily Report.
